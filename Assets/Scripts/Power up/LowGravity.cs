﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LowGravity : MonoBehaviour
{
    private Stopwatch timer;
    public int time = 10;
    public bool get = false;

    private void Start()
    {
        timer = new Stopwatch();
        timer.Start();
    }

    private void Update()
    {
        if (get == true)
        {
            GameObject.Find("Player").GetComponent<Rigidbody2D>().mass = 0.7f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            get = true;
            StartCoroutine(Duration(time));
            GameObject.Find("Player").GetComponent<Rigidbody2D>().mass = 0.7f;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
    IEnumerator Duration(float wait)
    {
        yield return new WaitForSeconds(wait);
        GameObject.Find("Player").GetComponent<Rigidbody2D>().mass = 1f;
        timer.Stop();
        GameManager.Instance.powerUpTime += (int)timer.Elapsed.TotalSeconds;
        Destroy(gameObject);
    }

}
