﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class UlleresPowerUp : MonoBehaviour
{

    public Stopwatch timer;
    private void Start()
    {
        timer = new Stopwatch();
        timer.Start();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            GameManager.Instance.GetUlleres(8);
            timer.Stop();
            GameManager.Instance.powerUpTime += (int)timer.Elapsed.TotalSeconds;
            Destroy(gameObject);
        }
    }


}
