﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    [SerializeField]
    float spawnProb;

    [SerializeField]
    GameObject [] powerUps;

    void Start()

    {
        int number;
        // Item a fer spawn
        if (GameManager.Instance.boss == true) {
           number = Random.Range(0, 3);
        } else
        {
            number = Random.Range(1, 3);
        }

        // Probabilitat de spawn
        float spawn = spawnProb / 100;
        if (Random.value < spawn)
        {
            Instantiate(powerUps[number], this.transform.position, Quaternion.identity);
        }
        // Destruir el objecte
        Destroy(gameObject);
    }

}
