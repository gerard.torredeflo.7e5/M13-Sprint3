﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CoinPowerUp : MonoBehaviour
{
    private Stopwatch timer;
    public int time = 10;
    public bool get = false;

    private void Start()
    {
        timer = new Stopwatch();
        timer.Start();
    }

    private void Update()
    {
        if (get == true)
        {
            GameManager.Instance.coinAttractor = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            get = true;
            StartCoroutine(Duration(time));
            GameManager.Instance.coinAttractor = true;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
    IEnumerator Duration(float wait)
    {
        yield return new WaitForSeconds(wait);
        GameManager.Instance.coinAttractor = false;
        timer.Stop();
        GameManager.Instance.powerUpTime += (int)timer.Elapsed.TotalSeconds;
        Destroy(gameObject);
    }
}
