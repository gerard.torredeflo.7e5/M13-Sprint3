﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridSpawnerBackSmall : MonoBehaviour
{

    GameObject[] grids;

    [SerializeField]
    public string carpeta;


    void Start()
    {
        grids = Resources.LoadAll(carpeta, typeof(GameObject)).Cast<GameObject>().ToArray();

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.generateGridBackSmall == true)
        {
            GameManager.Instance.restLayerSmall();
            GameObject obj = Instantiate(grids[Random.Range(0, grids.Length)], new Vector2(Random.RandomRange(19, 20.92f), 1), Quaternion.identity);
  
            TilemapRenderer[] tilemap = obj.GetComponentsInChildren<TilemapRenderer>();

            // Cambia a cada tilemap la Layer
            foreach (TilemapRenderer child in tilemap)
            {
                child.sortingOrder = child.sortingOrder + GameManager.Instance.backsmallLayer;
            }
            
            obj.transform.parent = this.transform;

            // Apaga per que no faci un altre grid al moment
            GameManager.Instance.generateGridBackSmall = false;
        }
    }
}
