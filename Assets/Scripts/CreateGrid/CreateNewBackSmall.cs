﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CreateNewBackSmall : MonoBehaviour
{
    private bool singleTime = true;
    public float tilesFront = 0;
    public float tilesBack = 0;
    void Update()
    {
        if (this.transform.position.x < 19 - (-tilesBack - tilesFront  + gameObject.GetComponentInChildren<Tilemap>().size.x) / 4 && singleTime == true)
        {
            GameManager.Instance.generateGridBackSmall = true;
            singleTime = false;
        }
        if (this.transform.position.x < -18)
        {
            Destroy(gameObject);
        }
    }
}
