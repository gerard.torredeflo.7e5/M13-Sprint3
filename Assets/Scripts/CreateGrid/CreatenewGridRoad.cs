﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CreatenewGridRoad : MonoBehaviour
{
    private bool singleTime = true;
    void Update()
    {
        if (this.transform.position.x < 20 - gameObject.GetComponentInChildren<Tilemap>().size.x && singleTime==true)
        {
            Debug.Log(gameObject.GetComponentInChildren<Tilemap>().size.x);
            GameManager.Instance.generateGridRoad = true;
            singleTime = false;
        }
        if (this.transform.position.x < -18)
        {
            Destroy(gameObject);
        }
    }
}
