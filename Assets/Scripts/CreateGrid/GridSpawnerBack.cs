﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridSpawnerBack : MonoBehaviour
{
    
    GameObject[] grids;

    [SerializeField]
    public string carpeta;


    void Start()
    {
        grids = Resources.LoadAll(carpeta, typeof(GameObject)).Cast<GameObject>().ToArray();

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.generateGridBack==true)
        {
            GameObject obj = Instantiate(grids[Random.Range(0,grids.Length)], new Vector2(Random.RandomRange(19, 20.48f), 0), Quaternion.identity);

            TilemapRenderer[] tilemap = obj.GetComponentsInChildren<TilemapRenderer>();

            // Cambia a cada tilemap la Layer
            foreach (TilemapRenderer child in tilemap)
            {
                child.sortingOrder = child.sortingOrder + GameManager.Instance.layer;
            }

            //Resta les capes per els següents grids
            GameManager.Instance.restLayer();
            obj.transform.parent = this.transform;

            // Apaga per que no faci un altre grid al moment
            GameManager.Instance.generateGridBack = false;
        }
    }
}
