﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;


public class GridSpawnerRoad : MonoBehaviour
{

    GameObject[] grids;

    [SerializeField]
    public string carpeta;


    void Start()
    {
        grids = Resources.LoadAll(carpeta, typeof(GameObject)).Cast<GameObject>().ToArray();

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.generateGridRoad == true)
        {
            GameObject obj = Instantiate(grids[Random.Range(0, grids.Length)], new Vector2(19.85f, 0), Quaternion.identity);
            
            obj.transform.parent = this.transform;
            GameManager.Instance.generateGridRoad = false;
        }
    }
}
