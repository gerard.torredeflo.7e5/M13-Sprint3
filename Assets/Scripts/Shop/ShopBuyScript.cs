﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBuyScript : MonoBehaviour
{
    [SerializeField]
    ShopItem shopItem;
    Text cost;
    bool claimed;
    int money;

    void Start()
    {
        cost = GetComponentInChildren<Text>();
        cost.text = shopItem.price.ToString();
    }

    void Update()
    {
        claimed = shopItem.claimed;
        money = GameManager.Instance.totalCoins;
        AlreadyClaimed();
    }

    public void BuyItem()
    {
        if (!claimed & (money >= shopItem.price))
        {
            shopItem.claimed = true;
            GameManager.Instance.totalCoins -= shopItem.price;
            if (shopItem.item == Item.Coin)
            {
                GameManager.Instance.coinValue += 1;
            }
            else if (shopItem.item == Item.Reload)
            {
                GameManager.Instance.reloadValue = 1;
            }
            else if (shopItem.item == Item.Ammo)
            {
                GameManager.Instance.ammoValue = 4;
            }
            else if (shopItem.item == Item.Shield)
            {
                GameManager.Instance.shieldSkillEnabled = 1;
            }
            else if (shopItem.item == Item.Machinegun)
            {
                GameManager.Instance.machinegunEnabled = 1;
            }
            else if (shopItem.item == Item.Nuke)
            {
                GameManager.Instance.bombSkillEnabled = 1;
            }
        }
    }

    void AlreadyClaimed()
    {
        if (shopItem.claimed)
        {
            cost.transform.parent.gameObject.GetComponent<Image>().color = Color.gray;
        }
    }
}