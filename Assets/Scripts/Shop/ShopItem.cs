﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Item { Coin, Reload, Ammo, Shield, Machinegun, Nuke }

[CreateAssetMenu(fileName = "New ShopItem", menuName = "ScriptableObjects/ShopItem", order = 1)]
public class ShopItem : ScriptableObject
{
    public string itemName;
    public Item item;
    public int price;
    public bool claimed;
}
