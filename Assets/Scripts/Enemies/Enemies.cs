﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    public float enemySpeed;
    public float enemyLifes;
    public int points;
    protected string[] colorTag =
    {
        "greenKiller",
        "redKiller",
        "blueKiller"
    };
    protected int assignedColor;

    protected virtual void Start()
    {
        assignedColor = Random.Range(0, 3);

        gameObject.GetComponent<Animator>().SetInteger("type", assignedColor);
        gameObject.GetComponent<Animator>().SetTrigger("New Trigger");
        Debug.Log("type " + assignedColor);
    }

    protected virtual void Update()
    {
        PartFromGameSpeed();
        EnemyStatusCheck();
    }

    protected virtual void PartFromGameSpeed()
    {
        transform.Translate(Vector3.left * (GameManager.Instance.speed) * Time.deltaTime);
    }

    protected virtual void EnemyStatusCheck()
    {
        if (enemyLifes <= 0)
        {
            GameManager.Instance.enemyPoints += points;
            GameManager.Instance.enemiesKilled += 1;
            if (GameManager.Instance.isInTheAir)
            {
                GameManager.Instance.enemyKilledWhileOnAir += 1;
            }
            Destroy(gameObject);
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == colorTag[assignedColor])
        {
            //Donat el moment canviar el 1 per la quantitat de damage que fa la bala que utilitzi el jugador
            enemyLifes -= 1;
        }
    }

    //Nomes s'utilitza per ara amb el soldier per el tamany del seu collider, pot ser modificat més endavant
    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == colorTag[assignedColor])
        {
            //Donat el moment canviar el 1 per la quantitat de damage que fa la bala que utilitzi el jugador
            enemyLifes -= 1;
        }
    }
}
