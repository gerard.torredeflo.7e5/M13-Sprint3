﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyEnemies : Enemies
{
    public float flyArc;
    public float originalHeight;
    public bool goUp;
    public float minRange;
    public float maxRange;

    protected override void Start()
    {
        base.Start();
        originalHeight = transform.position.y;
        goUp = (Random.value < 0.5);
        minRange = 0.3f;
        maxRange = 0.8f;
        flyArc = Random.Range(minRange, maxRange);
        if (flyArc < 0.5f) enemySpeed = 1;
        if (flyArc > 0.5f) enemySpeed = 2;
    }

    protected override void Update()
    {
        base.Update();
        ChangeDirection();
        Fly();
    }

    protected virtual void ChangeDirection()
    {
        if (transform.position.y >= originalHeight + flyArc)
        {
            goUp = false;
        }

        if (transform.position.y <= originalHeight - flyArc)
        {
            goUp = true;
        }
    }

    protected virtual void Fly()
    {
        if (goUp == true)
        {
            transform.Translate(Vector3.up * enemySpeed * Time.deltaTime);
        }

        if (goUp == false)
        {
            transform.Translate(Vector3.down * enemySpeed * Time.deltaTime);
        }
    }
}
