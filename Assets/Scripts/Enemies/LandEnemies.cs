﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandEnemies : Enemies
{
    public bool enemyLanded = false;

    protected override void Update()
    {
        base.Update();
        StartMoving();
    }

    protected virtual void StartMoving()
    {
        if (enemyLanded)
        {
            transform.Translate(Vector3.left * enemySpeed * Time.deltaTime);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        enemyLanded = true;
        base.OnTriggerEnter2D(collision);
    }
}
