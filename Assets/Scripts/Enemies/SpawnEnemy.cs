﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField]
    float spawnProb;

    [SerializeField]
    GameObject enemy;
    // Start is called before the first frame update
    void Start()
    {
        float spawn = spawnProb / 100;
        if (Random.value < spawn)
        {
            Instantiate(enemy, this.transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }

    
}
