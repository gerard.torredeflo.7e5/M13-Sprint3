﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skymine : SkyEnemies
{
    [SerializeField]
    GameObject prefabExplosion;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void EnemyStatusCheck()
    {
        base.EnemyStatusCheck();
        if (enemyLifes <= 0)
        {
            Instantiate(prefabExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
