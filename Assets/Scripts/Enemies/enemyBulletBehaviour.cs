﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBulletBehaviour : MonoBehaviour
{
    private int Type;

    // Start is called before the first frame update
    void Start()
    {
        Type = Random.Range(1, 4);
        gameObject.GetComponent<Animator>().SetInteger("type", Type);
        gameObject.GetComponent<Animator>().SetTrigger("New Trigger");
        Debug.Log("type " + Type);

        if (Type == 1)
        {
            gameObject.tag = "blue";
        }
        else if (Type == 2)
        {
            gameObject.tag = "red";
        }
        else if (Type == 3)
        {
            gameObject.tag = "green";
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
