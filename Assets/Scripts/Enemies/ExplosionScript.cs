﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{
    [SerializeField]
    float explosionDuration;

    void Start()
    {
        StartCoroutine(TimeOnScreen());
    }

    private void Update()
    {
        //Mantenir la posició respecte al escenari en moviment
        transform.Translate(Vector3.left * (GameManager.Instance.speed) * Time.deltaTime);
    }

    IEnumerator TimeOnScreen()
    {
        //La duració de l'explosió en pantalla
        yield return new WaitForSeconds(explosionDuration);
        Destroy(gameObject);
    }
}
