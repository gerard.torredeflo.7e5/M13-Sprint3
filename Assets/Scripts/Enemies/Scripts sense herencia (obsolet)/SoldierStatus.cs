﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierStatus : MonoBehaviour
{
    bool isAlive = true;

    private void Update()
    {
        enemyStatusCheck();
    }

    void enemyStatusCheck()
    {
        if (isAlive == false)
        {
            Destroy(gameObject);
        }
    }

    //Si entra en contacte amb el collider d'un GameObject amb tag "Bullet", mor
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            isAlive = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            isAlive = false;
        }
    }
}
