﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkymineStatus : MonoBehaviour
{
    [SerializeField]
    GameObject prefabExplosion;
    bool isAlive = true;

    private void Update()
    {
        enemyStatusCheck();
    }

    void enemyStatusCheck()
    {
        if (isAlive == false)
        {
            Instantiate(prefabExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    //Si entra en contacte amb el trigger collider d'un GameObject amb tag "Bullet" instancia la explosió i desapareix
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            isAlive = false;
        }
    }
}
