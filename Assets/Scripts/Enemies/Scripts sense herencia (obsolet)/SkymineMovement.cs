﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkymineMovement : MonoBehaviour
{
    float flyArc;
    public float flySpeed;
    float originalHeight;
    bool goUp;

    private void Start()
    {
        //Recull la altura inicial
        originalHeight = transform.position.y;

        //La direcció d'inici es definida aleatoriament
        goUp = (Random.value < 0.5);

        //L'arc de desplaçament aleatori en el que es pot moure la mina, definit aleatoriament entre dos valors
        flyArc = Random.Range(0.3f, 0.8f);

        //Segons la distància de l'arc es mou dins d'aquest a més o menys velocitat
        if (flyArc < 0.5f) flySpeed = 1;
        if (flyArc > 0.5f) flySpeed = 2;
    }

    void Update()
    {
        //Seguir el ritme de les plataformes
        transform.Translate(Vector3.left * (GameManager.Instance.speed) * Time.deltaTime);

        //Arribada una distància determinada per flyArc canvia la direcció del vol
        ChangeDirection();

        //Es desplaça amunt o avall segons goUp
        Fly();
    }

    void ChangeDirection()
    {
        if (transform.position.y >= originalHeight + flyArc)
        {
            goUp = false;
        }

        if (transform.position.y <= originalHeight - flyArc)
        {
            goUp = true;
        }
    }

    void Fly()
    {
        if (goUp == true)
        {
            transform.Translate(Vector3.up * flySpeed * Time.deltaTime);
        }

        if (goUp == false)
        {
            transform.Translate(Vector3.down * flySpeed * Time.deltaTime);
        }
    }
}
