﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierMovement : MonoBehaviour
{
    public float enemySpeed;
    bool enemyLanded = false;
    void Start()
    {
        //Ritme de la partida
        enemySpeed += GameManager.Instance.speed;
    }

    void Update()
    {
        if (enemyLanded == true)
        {
            StartMoving();
        }
    }

    //Al tocar el terra comença a desplaçar-se
    private void OnTriggerEnter2D(Collider2D collision)
    {
        enemyLanded = true;
    }

    void StartMoving()
    {
        transform.Translate(Vector3.left * enemySpeed * Time.deltaTime);
    }
}
