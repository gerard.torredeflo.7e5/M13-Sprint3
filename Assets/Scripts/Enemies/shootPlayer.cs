﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootPlayer : MonoBehaviour
{
    public GameObject self;

    float angle;
    public GameObject bullet;
    public Transform puntDeDispar;
    public Transform weapon;
    //public Transform attacker;
    // public Transform offset;

    public float bulletSpeed = 10;

    public float timeToShoot = 1;
    private float nextFire = 0f;

    public Vector3 offset = new Vector3(-0.45f, -1.75f, 0);


    // Update is called once per frame
    void Update()
    {

        if (GetComponent<Boss>().shootEnabled)
        {

            Transform target = GameObject.FindGameObjectsWithTag("Player")[0].transform;

            Vector2 lookdir = puntDeDispar.position - target.position;
            angle = Mathf.Atan2(lookdir.y, lookdir.x) * Mathf.Rad2Deg;

            //weapon.rotation = angle - 30;
            Quaternion rotation = Quaternion.Euler(0, angle - 30, 0);
            weapon.transform.localPosition = offset;

            if (Time.time > nextFire)
            {

                GameObject bulletClone = Instantiate(bullet);

                //Physics.IgnoreCollision(bulletClone.GetComponent<Collider>(), self.GetComponent<Collider>(), false);

                bulletClone.transform.position = puntDeDispar.position;
                bulletClone.transform.rotation = Quaternion.Euler(0, 0, angle + 90);

                bulletClone.GetComponent<Rigidbody2D>().velocity = puntDeDispar.right * bulletSpeed;




                nextFire = Time.time + timeToShoot;

            }
        }
    }
}
