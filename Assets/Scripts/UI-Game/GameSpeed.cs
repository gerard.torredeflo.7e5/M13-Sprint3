﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpeed : MonoBehaviour
{
    //Velocitat de la partida reflexat en la velocitat a la que les plataformes es desplaçen
    float speed;

    void Update()
    {
        speed = GameManager.Instance.speed;
        transform.Translate(Vector3.left * speed * Time.deltaTime);
    }
}
