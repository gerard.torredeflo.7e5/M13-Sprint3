﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceDone : MonoBehaviour
{
    private Text textDistance;
    private int distance;

    private void Start()
    {
        textDistance = gameObject.GetComponent<Text>();
        GameManager.Instance.distance = 0;
    }
    void Update()
    {
        textDistance.text = "" + Mathf.RoundToInt(-GameObject.Find("Inici").transform.position.x);
        distance = Mathf.RoundToInt(-GameObject.Find("Inici").transform.position.x);
        GameManager.Instance.distance = distance;
    }
}
