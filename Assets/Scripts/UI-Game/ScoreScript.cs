﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    [SerializeField]
    private GameObject distanceGO;
    [SerializeField]
    private GameObject enemiesGO;
    [SerializeField]
    private GameObject coinsGO;
    [SerializeField]
    private GameObject scoreGO;

    private int distance;
    private int enemiesKilled;
    private int enemyPoints;
    private int collectedCoins;
    private int finalScore;

    void Start()
    {
        distance = GameManager.Instance.distance;
        enemiesKilled = GameManager.Instance.enemiesKilled;
        enemyPoints = GameManager.Instance.enemyPoints;
        collectedCoins = GameManager.Instance.collectedCoins;
        GameManager.Instance.totalEnemiesKilled += enemiesKilled;

        distanceGO.GetComponent<Text>().text = "" + distance;
        enemiesGO.GetComponent<Text>().text = "" + enemiesKilled;
        coinsGO.GetComponent<Text>().text = "" + collectedCoins;
        ScoreMath();
        scoreGO.GetComponent<Text>().text = "" + finalScore;

        GameManager.Instance.totalCoins += collectedCoins;
    }

    private void ScoreMath()
    {
        finalScore = distance * 2;
        finalScore += enemyPoints;
        finalScore += collectedCoins * 2;
    }
}
