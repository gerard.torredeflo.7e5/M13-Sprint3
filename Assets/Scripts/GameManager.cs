﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    [SerializeField]
    public float speed = 2;

    public bool generateGridRoad = true;
    public bool generateGridBack = true;
    public bool generateGridBackSmall = true;
    public float disDeathY = -4.5f;
    public float disDeathX = -8f;
    public int layer = 0;
    public int backsmallLayer = -16000;
    public int colour = 1;
    public int lifes = 3;

    public bool coinAttractor = false;
    public bool ulleres = false;
    public bool boss = false;

    public int distance;
    public int totalEnemiesKilled;
    public int enemiesKilled;
    public int enemyPoints;
    public int collectedCoins;
    public int totalCoins;
    public int timesBossKilled;
    public int missilesDestroyed;
    public int shieldUsed;
    public int buyedItems;
    public int bossTimeOnScreen;
    public int blindTime;
    public int powerUpTime;
    public int lowgravSkillTime;
    public int enemyKilledWhileOnAir;
    public bool bossWin;
    public int revengeAccomplished;
    public int bombSkillEnabled = 0;
    public int machinegunEnabled = 0;
    public int shieldSkillEnabled = 0;
    public int coinValue = 0;
    public int reloadValue = 0;
    public int ammoValue = 0;

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //El valor públic de l'estat de jugador segons si està al aire o no
    public bool isInTheAir = false;

    //Mort i animació de mort del player
    public void DeathAnimation(GameObject ch)
    {
        StartCoroutine(Blink(2, ch));
        lifes--;

    }

    IEnumerator Blink(float waitTime, GameObject ch)
    {
        var endTime = Time.time + waitTime;

        yield return new WaitForSeconds(0.1f);
        ch.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(0.3f);
        ch.GetComponent<SpriteRenderer>().enabled = true;
        if (lifes == 0)
        {
            GameObject.FindGameObjectWithTag("SceneManagement").GetComponent<SceneRelatedUI>().LoadScore();
            lifes = 3;
        }

    }

    public void restLayer()
    {
        layer--;
        layer--;

    }

    public void restLayerSmall()
    {
        backsmallLayer--;
        backsmallLayer--;

    }

    public void GetUlleres(float time)
    {
        ulleres = true;
        StartCoroutine(Duration(time));
    }

    IEnumerator Duration(float wait)
    {
        yield return new WaitForSeconds(wait);
        ulleres = false;
    }
}
