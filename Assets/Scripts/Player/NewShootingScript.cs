﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewShootingScript : MonoBehaviour
{
    public int colour = 0;

    float anlge;
    public GameObject bullet;
    public Camera mainCamera;

    public Transform puntDeDispar;
    public Rigidbody2D rb;
    public Rigidbody2D Center;
    public float bulletSpeed = 10;
    public float knockBack = 10000f;

    public bool shootAvailable;
    public float cdShoot = 0.5f;
    public int municioMax = 12;
    public int municio = 12;
    public float reloadTime = 2;
    public bool reload = false;

    private void Start()
    {
        shootAvailable = true;
        municio += GameManager.Instance.ammoValue;
        municioMax += GameManager.Instance.ammoValue;
        reloadTime -= GameManager.Instance.reloadValue;
    }

    void Update()
    {
        Vector2 mouseWorldPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 lookDir = mouseWorldPosition - rb.position;
        anlge = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg -3.5f;
        Center.rotation = anlge;
        Center.position = rb.position;
        //Debug.Log(mouseWorldPosition);
        if (reload == false && (municio <= 0 || Input.GetKeyDown(KeyCode.R)))
        {
            shootAvailable = false;
            StartCoroutine(ReloadTime(reloadTime));
            reload = true;
        }

        if (Input.GetMouseButtonDown(0) && shootAvailable == true && reload == false && mouseWorldPosition.y > -5.1f)
        {
            GameObject bulletClone = Instantiate(bullet);
            bulletClone.transform.position = puntDeDispar.position;
            bulletClone.transform.rotation = Quaternion.Euler(0, 0, anlge - 90);

            bulletClone.GetComponent<Rigidbody2D>().velocity = puntDeDispar.right * bulletSpeed;

            bulletClone.GetComponent<BulletBehaviour>().changeColour(colour);

            municio--;
            shootAvailable = false;
            StartCoroutine(CdShoot(cdShoot));
            //Impulse self back on shoot
            if (GameManager.Instance.isInTheAir == true)
            {
                rb.AddForce(lookDir.normalized * -knockBack, ForceMode2D.Force);
            }
        }

        IEnumerator CdShoot(float wait)
        {
            yield return new WaitForSeconds(wait);
            shootAvailable = true;

        }

        IEnumerator ReloadTime(float wait)
        {
            yield return new WaitForSeconds(wait);
            shootAvailable = true;
            reload = false;
            municio = municioMax;

        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            colour--;
            if (colour <= -1)
            {
                colour = 2;
            }
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            colour++;
            if (colour >= 3)
            {
                colour = 0;
            }
        }

    }



}

