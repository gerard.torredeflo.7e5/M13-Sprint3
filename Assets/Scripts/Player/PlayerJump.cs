﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [SerializeField]
    int jumpForce;
    Rigidbody2D playerRigidbody;


    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        GameManager.Instance.isInTheAir = false;
    }

    void Update()
    {
        //Dos tipus de salt amb teclat
        JumpDirections();

    }

    void JumpDirections()
    {
        if ((Input.GetKeyDown(KeyCode.UpArrow) | Input.GetKeyDown(KeyCode.W)) & GameManager.Instance.isInTheAir == false)
        {
            playerRigidbody.AddForce(transform.up * jumpForce);
            GameManager.Instance.isInTheAir = true;
        }

        /*if ((Input.GetKeyDown(KeyCode.RightArrow) | Input.GetKeyDown(KeyCode.D)) & GameManager.Instance.isInTheAir == false)
        {
            playerRigidbody.AddForce(transform.up * jumpForce);
            playerRigidbody.AddForce(transform.right * jumpForce / 2);
            GameManager.Instance.isInTheAir = true;
        }*/
    }

    //Si toca el terra no pot saltar
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PickUp")
        {
            
        }
        else
        {
            GameManager.Instance.isInTheAir = false;
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PickUp")
        {

        }
        else
        {
            GameManager.Instance.isInTheAir = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PickUp")
        {

        }
        else
        {
            GameManager.Instance.isInTheAir = true;
        }
    }
}
