﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowBulletColor : MonoBehaviour
{
    [SerializeField]
    Color[] color;
    public Sprite[] colorious;
    SpriteRenderer spriteRenderer;
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {

        int bulletColor = GetComponentInParent<NewShootingScript>().colour;
        spriteRenderer.sprite = colorious[bulletColor];
    }
}
