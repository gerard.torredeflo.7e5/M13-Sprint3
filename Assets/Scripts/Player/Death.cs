﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    private void Start()
    {
        GameManager.Instance.enemyPoints = 0;
        GameManager.Instance.enemiesKilled = 0;
        GameManager.Instance.collectedCoins = 0;
    }
    string[] damageTags = { "green", "blue", "red", "Enemy" };

    private bool hit = false;

    public static bool shield = false;
    //public int _deathCount = 2; TODO TRY-------------------------------------------------
    void Update()
    {
        PlayerOutOfScreen();

        if (hit == true)
        {
            GameManager.Instance.DeathAnimation(gameObject);
            hit = false;
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            shieldActive(2);
        }
    }

    //Si el player surt de la pantalla, mor
    void PlayerOutOfScreen()
    {
        if (gameObject.transform.position.y < GameManager.Instance.disDeathY && hit == false || gameObject.transform.position.x < GameManager.Instance.disDeathX && hit == false)
        {
            hit = true;
            transform.position = new Vector2(-4, 3);
        }
    }

    //Al entrar en contacte amb qualsevol tipus de collider d'un GameObject amb el tag "Enemic", el player perd 1 vida
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy" && shield == false)
        {
            hit = true;
            GameManager.Instance.bossWin = false;
            transform.position = new Vector2(-4, 3);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (shield == false && (collision.gameObject.CompareTag("green") || collision.gameObject.CompareTag("blue") || collision.gameObject.CompareTag("red") || collision.gameObject.CompareTag("Enemy")))
        {
            hit = true;
            GameManager.Instance.bossWin = true;
            transform.position = new Vector2(-4, 3);
        }
    }

    public void shieldActive(float time)
    {
        shield = true;
        GameManager.Instance.shieldUsed += 1;
        StartCoroutine(Shield(time));
    }

    IEnumerator Shield(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        shield = false;
    }
}
