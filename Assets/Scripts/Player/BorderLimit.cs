﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderLimit : MonoBehaviour
{

    void Update()
    {
        // No deixa sortir de 2 limits
        Vector3 minScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        Vector3 maxScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minScreenBounds.x, maxScreenBounds.x - 1), Mathf.Clamp(transform.position.y, minScreenBounds.y, maxScreenBounds.y), transform.position.z);

        // Treu la força al tocar la barrera
        if (gameObject.transform.position.y >= 4)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(GetComponent<Rigidbody2D>().velocity.x,0,0);
        }

        if (gameObject.transform.position.x >= 7.5)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(-0.5f, GetComponent<Rigidbody2D>().velocity.y, 0);
        }
    }
}
