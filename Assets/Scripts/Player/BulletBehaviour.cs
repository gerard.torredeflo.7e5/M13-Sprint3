﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public SpriteRenderer sr;
    public Sprite[] bullets;

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public void changeColour(int Colour)
    {
        if (Colour == 0)
        {
            transform.gameObject.tag = "greenKiller";
            sr.sprite = bullets[0];
        }
        else if (Colour == 1)
        {
            transform.gameObject.tag = "redKiller";
            sr.sprite = bullets[1];
        }
        else if (Colour == 2)
        {
            transform.gameObject.tag = "blueKiller";
            sr.sprite = bullets[2];
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "green" && gameObject.tag == "greenKiller")
        {
            GameManager.Instance.missilesDestroyed += 1;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "red" && gameObject.tag == "redKiller")
        {
            GameManager.Instance.missilesDestroyed += 1;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "blue" && gameObject.tag == "blueKiller")
        {
            GameManager.Instance.missilesDestroyed += 1;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag != "PickUp")
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "PickUp")
        {
            Destroy(gameObject);
        }
    }
}
