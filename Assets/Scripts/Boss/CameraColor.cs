﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Wilberforce;

public class CameraColor : MonoBehaviour
{
    private Stopwatch timer;
    public int color;
    private float nextFire = 0f;
    public float firerate = 3;

    // Start is called before the first frame update
    void Start()
    {
        color = Random.Range(1, 4);
        timer = new Stopwatch();
        timer.Start();
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Main Camera").GetComponent<Colorblind>().Type = color;
        if (GameManager.Instance.ulleres == true)
        {
            GameObject.Find("Main Camera").GetComponent<Colorblind>().Type = 0;
            timer.Stop();
            GameManager.Instance.blindTime += (int)timer.Elapsed.TotalSeconds;
        }
        if (Time.time > nextFire)
        {
            changeColour();
        }
        

    }
    void changeColour()
    {
        nextFire = Time.time + firerate;
        color = Random.Range(1, 4);
    }

}
