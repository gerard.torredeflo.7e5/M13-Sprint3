﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using Wilberforce;

public class Boss : SkyEnemies
{
    private Stopwatch timer;
    public bool shootEnabled;
    [SerializeField]
    GameObject prefabSolider;
    [SerializeField]
    GameObject prefabSkymine;
    [SerializeField]
    float enemySpawnDelay;
    bool spawn;
    [SerializeField]
    float soldierAltitudeSpawn;
    float skymineAltitudeSpawn;
    [SerializeField]
    float skymineAltitudeSpawnMin;
    [SerializeField]
    float skymineAltitudeSpawnMax;

    protected override void Start()
    {
        base.Start();
        spawn = true;
        timer = new Stopwatch();
        timer.Start();
        GameManager.Instance.boss = true;
    }

    protected override void Update()
    {
        //transform.position = new Vector3(0, 1, 0) * Time.deltaTime;
        if (transform.position.x >= 6)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, 0);
        } else {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

            base.Update();
            if (enemyLifes > 15)
            {
                shootEnabled = false;
                SpawnEnemies();
            }
            else if (enemyLifes > 10 & enemyLifes <= 15)
            {
                shootEnabled = true;
            }
            else if (enemyLifes <= 10)
            {
                shootEnabled = true;
                SpawnEnemies();
            }
        }
        //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0);



    }

    protected override void EnemyStatusCheck()
    {
        
        if (enemyLifes <= 0)
        {
            timer.Stop();
            GameManager.Instance.bossTimeOnScreen = (int)timer.Elapsed.TotalSeconds;
            GameManager.Instance.timesBossKilled += 1;
            if (GameManager.Instance.bossWin)
            {
                GameManager.Instance.revengeAccomplished += 1;
            }
            GameManager.Instance.lifes = 3;
            GameObject.FindGameObjectWithTag("SceneManagement").GetComponent<SceneRelatedUI>().LoadScore();
        }
        base.EnemyStatusCheck();
    }

    protected override void PartFromGameSpeed()
    {
        //Es queda estatic al lloc (TEST)
        base.PartFromGameSpeed();
        transform.Translate(Vector3.left * (-2) * Time.deltaTime);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (string element in colorTag)
        {
            if (collision.gameObject.tag == element)
            {
                enemyLifes -= 1;
            }
        }
    }

    void SpawnEnemies()
    {
        if (spawn)
        {
            Vector3 spawnLocation = transform.position;
            if (Random.value > 0.5)
            {
                spawnLocation.y -= soldierAltitudeSpawn;
                Instantiate(prefabSolider, spawnLocation, Quaternion.identity);
            }
            else
            {
                skymineAltitudeSpawn = Random.Range(skymineAltitudeSpawnMin, skymineAltitudeSpawnMax);
                spawnLocation.y -= skymineAltitudeSpawn;
                Instantiate(prefabSkymine, spawnLocation, Quaternion.identity);
            }
            StartCoroutine(SpawnDelay());
        }
    }

    IEnumerator SpawnDelay()
    {
        spawn = false;
        yield return new WaitForSeconds(enemySpawnDelay);
        spawn = true;
    }
}
