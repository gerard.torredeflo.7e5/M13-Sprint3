﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBoss : MonoBehaviour
{
    public GameObject boss;
    public int nextMilestone;
    private bool spawnable;
    public bool respawnable;
    void Start()
    {
        spawnable = true;
        
    }

    void Update()
    {
        if (spawnable == true && GameManager._instance.distance == nextMilestone)
        {
            //Instantiate(boss, new Vector3(13F,8F,0F));
            Instantiate(boss, new Vector3(13F, 1.5F, 0F), Quaternion.identity);

            if (!respawnable)
            {
                spawnable = false;
            }
        }
    }
}
