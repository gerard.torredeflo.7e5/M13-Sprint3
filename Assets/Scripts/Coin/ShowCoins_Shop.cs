﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowCoins_Shop : MonoBehaviour
{
    Text textComponent;
    void Start()
    {
        textComponent = GetComponent<Text>();
    }

    void Update()
    {
        textComponent.text = "" + GameManager.Instance.totalCoins;
    }
}
