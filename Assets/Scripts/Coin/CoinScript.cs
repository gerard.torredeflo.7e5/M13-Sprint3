﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{

    private void Update()
    {
        transform.Translate(Vector3.left * (GameManager.Instance.speed) * Time.deltaTime);

        if (GameManager.Instance.coinAttractor == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("Player").GetComponent<Transform>().position, 1 * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.collectedCoins += GameManager.Instance.coinValue;
            Destroy(gameObject);
        }
    }
}
