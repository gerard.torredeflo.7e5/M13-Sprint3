﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AchievementType
{
    Enemies, Distance, Boss, Missiles, Blindness, Coins, Shop, PowerUps, Skills
}

public enum RequiredValue
{
    Amount, Time, AirKill, Revenge
}

[CreateAssetMenu(fileName = "New Achievement", menuName = "ScriptableObjects/Achievement", order = 1)]
public class Achievements : ScriptableObject
{
    public string title;
    public string requirements;
    public AchievementType achievementType;
    public RequiredValue requiredValueType;
    public int requiredValue;
    public int reward;
    public bool claimable;
    public bool claimed;
}
