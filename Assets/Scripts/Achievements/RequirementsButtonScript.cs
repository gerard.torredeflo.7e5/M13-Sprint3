﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequirementsButtonScript : MonoBehaviour
{
    [SerializeField]
    GameObject achievementsContent;
    [SerializeField]
    GameObject achievementsRequirements;
    bool activeReq;
    ScrollRect scrollView;

    void Start()
    {
        scrollView = transform.parent.GetComponent<ScrollRect>();
        activeReq = false;
    }

    void Update()
    {
        ShowRequirements();
    }

    public void RequirementsButton()
    {
        if (activeReq) activeReq = false;
        else if (!activeReq) activeReq = true;
    }

    void ShowRequirements()
    {
        if (activeReq)
        {
            achievementsContent.SetActive(false);
            achievementsRequirements.SetActive(true);
            scrollView.content = achievementsRequirements.GetComponent<RectTransform>();
        }
        else if (!activeReq)
        {
            achievementsContent.SetActive(true);
            achievementsRequirements.SetActive(false);
            scrollView.content = achievementsContent.GetComponent<RectTransform>();
        }
    }
}
