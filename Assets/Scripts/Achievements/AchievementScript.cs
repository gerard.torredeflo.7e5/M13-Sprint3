﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementScript : MonoBehaviour
{
    public Achievements achievement;
    Text achText;
    Text claimText;
    void Start()
    {
        //TEXT
        SetText();

        //FUNCIONALITAT
        AchFunctionality();
    }

    void Update()
    {
        //MODIFICACIÓ DE COLOR PER ALS RECLAMATS
        AlreadyClaimed();
    }

    void SetText()
    {
        if (transform.parent.name == "Content")
        {
            achText = GetComponentInChildren<Text>();
            achText.text = achievement.title;
            claimText = GetComponentInChildren<Button>().GetComponentInChildren<Text>();
            claimText.text = "CLAIM (" + achievement.reward.ToString() + ")";
        }
        else if (transform.parent.name == "ContentRequirements")
        {
            achText = transform.Find("AchTitle").GetComponentInChildren<Text>();
            achText.text = achievement.title;
            claimText = transform.Find("AchReq").GetComponent<Text>();
            claimText.text = achievement.requirements;
        }
    }

    void AchFunctionality()
    {
        //Millorable guardant més variables i utilitzant switch
        RequiredValue reqValType = achievement.requiredValueType;
        AchievementType achType = achievement.achievementType;
        int requiredValue = achievement.requiredValue;

        //PER VALOR DE QUANTITAT
        if (reqValType == RequiredValue.Amount)
        {
            if (achType == AchievementType.Enemies)
            {
                if (GameManager.Instance.totalEnemiesKilled >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Distance)
            {
                if (GameManager.Instance.distance >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Boss)
            {
                if (GameManager.Instance.timesBossKilled >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Missiles)
            {
                if (GameManager.Instance.missilesDestroyed >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Coins)
            {
                if (GameManager.Instance.collectedCoins >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Skills)
            {
                if (GameManager.Instance.shieldUsed >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Shop)
            {
                if (GameManager.Instance.buyedItems >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
        }

        //PER VALOR DE TEMPS
        else if (reqValType == RequiredValue.Time)
        {
            if (achType == AchievementType.Boss)
            {
                if (GameManager.Instance.bossTimeOnScreen >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Blindness)
            {
                if (GameManager.Instance.blindTime >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.PowerUps)
            {
                if (GameManager.Instance.powerUpTime >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
            else if (achType == AchievementType.Skills)
            {
                if (GameManager.Instance.lowgravSkillTime >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
        }

        //CONDICIONS ESPECIALS
        //--------------------
        //MATAR DES DE L'AIRE
        else if (reqValType == RequiredValue.AirKill)
        {
            if (achType == AchievementType.Enemies)
            {
                if (GameManager.Instance.enemyKilledWhileOnAir >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
        }

        //MATAR DE TORNADA AL BOSS (Venjança)
        else if (reqValType == RequiredValue.Revenge)
        {
            if (achType == AchievementType.Boss)
            {
                if (GameManager.Instance.revengeAccomplished >= requiredValue)
                {
                    achievement.claimable = true;
                }
            }
        }
    }

    void AlreadyClaimed()
    {
        if (achievement.claimed)
        {
            if (transform.parent.name == "Content")
            {
                claimText = GetComponentInChildren<Button>().GetComponentInChildren<Text>();
                claimText.color = Color.gray;
            }
        }
    }
}
