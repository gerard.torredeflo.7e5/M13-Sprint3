﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaimScript : MonoBehaviour
{
    [SerializeField]
    private bool claimable;
    [SerializeField]
    private bool claimed;
    private AchievementScript achScript;

    void Start()
    {
        achScript = transform.parent.GetComponent<AchievementScript>();
    }

    void Update()
    {
        claimable = achScript.achievement.claimable;
        claimed = achScript.achievement.claimed;
    }

    public void ClaimReward()
    {
        if (claimable & !claimed)
        {
            Debug.Log("Logro reclamado");
            achScript.achievement.claimed = true;
            GameManager.Instance.totalCoins += achScript.achievement.reward;
        }
    }
}
