﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneRelatedUI : MonoBehaviour
{
    SceneManagement scene = new SceneManagement();

    public void LoadMenu()
    {
        scene.CurrentScene = SceneManagement.Scenes.Menu;
    }

    public void LoadShop()
    {
        scene.CurrentScene = SceneManagement.Scenes.Shop;
    }

    public void LoadAchievements()
    {
        scene.CurrentScene = SceneManagement.Scenes.Achievements;
    }

    public void LoadGame()
    {
        GameManager.Instance.generateGridRoad = true;
        GameManager.Instance.generateGridBack = true;
        GameManager.Instance.speed = 2;
        scene.CurrentScene = SceneManagement.Scenes.Game;
    }

    public void LoadScore()
    {
        scene.CurrentScene = SceneManagement.Scenes.Score;
    }
}
