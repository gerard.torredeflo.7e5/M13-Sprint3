﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombSkill : MonoBehaviour
{
    public int uses = 0;
    private void Start()
    {
        uses += GameManager.Instance.bombSkillEnabled;
    }

    public void Bomb()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
            GameObject.Destroy(enemy);

        uses--;
        if (uses == 0)
        {
            GameObject.Find("Skill1").GetComponent<Button>().interactable = false;
        }
    }

}
