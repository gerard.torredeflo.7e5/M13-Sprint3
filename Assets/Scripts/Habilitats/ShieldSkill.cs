﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldSkill : MonoBehaviour
{
    public int uses = 0;

    private void Start()
    {
        uses += GameManager.Instance.shieldSkillEnabled;
    }
    public void Shield()
    {
        GameObject.Find("Player").GetComponent<Death>().shieldActive(6);
        uses--;
        if (uses == 0)
        {
            GameObject.Find("Skill2").GetComponent<Button>().interactable = false;
        }
    }

}
