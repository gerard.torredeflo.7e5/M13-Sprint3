﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MachineGunSkill : MonoBehaviour
{
    public float cdOriginal;
    public int munOriginal;
    public float cdNou;
    public int municioNou;
    public float time = 10;

    public int uses = 0;

    private void Start()
    {
        uses += GameManager.Instance.machinegunEnabled;
    }
    public void Gun()
    {
        cdOriginal = GameObject.Find("Player").GetComponent<NewShootingScript>().cdShoot;
        munOriginal = GameObject.Find("Player").GetComponent<NewShootingScript>().municioMax;
        municioNou = GameObject.Find("Player").GetComponent<NewShootingScript>().municioMax * 2;

        GameObject.Find("Player").GetComponent<NewShootingScript>().cdShoot = GameObject.Find("Player").GetComponent<NewShootingScript>().cdShoot / 3;
        GameObject.Find("Player").GetComponent<NewShootingScript>().municioMax = municioNou;
        GameObject.Find("Player").GetComponent<NewShootingScript>().municio = municioNou;

        StartCoroutine(Duration(time));

        uses--;
        if (uses == 0)
        {
            GameObject.Find("Skill3").GetComponent<Button>().interactable = false;
        }
    }

    IEnumerator Duration(float time)
    {
        yield return new WaitForSeconds(time);
        GameObject.Find("Player").GetComponent<NewShootingScript>().cdShoot = cdOriginal;
        GameObject.Find("Player").GetComponent<NewShootingScript>().municioMax = munOriginal;
        GameObject.Find("Player").GetComponent<NewShootingScript>().municio = munOriginal;
    }

}
